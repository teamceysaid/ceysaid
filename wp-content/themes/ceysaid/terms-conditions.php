<?php
/**
 * Created by PhpStorm.
 * User: gihan
 * Date: 12/24/2017
 * Time: 8:49 PM
 */
/**
 * Template Name: Terms and  Conditions
 */
get_header();
?>
<div class="columns-container cms-about-us" id="cms">
    <div id="columns" class="container-fluid">

        <div id="slider_row" class="row">
        </div>
        <div class="row">
            <div id="center_column" class="center_column col-xs-12 col-sm-12">
                <ul id="home-page-tabs" class="nav nav-tabs clearfix">
                    <li class="active"><a data-toggle="tab" href="#" class="newarrivals">Terms and Conditions</a></li>
                </ul>
                <div class="tab-content"></div>
                <div class="rte">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <div class="cms-block">
                                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                                    <?php the_content(); ?>
                                <?php endwhile; endif;?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
