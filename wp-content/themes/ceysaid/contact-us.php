<?php
/**
 * Created by PhpStorm.
 * User: gihan
 * Date: 12/24/2017
 * Time: 2:29 PM
 */
/**
 * Template Name: Contact-us
 */
get_header();
?>
<div class="columns-container cms-about-us" id="cms">
    <div id="columns" class="container-fluid">

        <div id="slider_row" class="row">
        </div>
        <div class="row">
            <div id="center_column" class="center_column col-xs-12 col-sm-12">
                <ul id="home-page-tabs" class="nav nav-tabs clearfix">
                    <li class="active"><a data-toggle="tab" href="#" class="newarrivals">Contact us</a></li>
                </ul>
                <div class="tab-content"></div>
                <div class="rte">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <div class="cms-block">
                                <h3 class="page-subheading">Send your inquiry</h3>
                                <p>
                                <div id="contact_form_id">
                                    <?php echo do_shortcode( '[contact-form-7 id="5" title="Contact form 1" html_id="contact_form34"]' ); ?>
                                </div>
                                </p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4" id="parallax-image" style="background-image: url('<?php the_field("background_image")?>')">
                            <div class="cms-box"></div>
                            <div class="cms-about-overlay"></div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="cms-box">
                                <h3 class="page-subheading">Location</h3>
                                <div class="col-md-4" id="location_details">
                                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> <?php the_field('location')?></p>
                                    <p>
                                        <?php if(have_rows('email')):
                                                    while (have_rows('email')): the_row();
                                            ?>
                                                       <a href="mailto:<?php the_sub_field('email')?>"><i class="fa fa-envelope" aria-hidden="true"></i> <?php the_sub_field('email')?></a> <br>
                                        <?php       endwhile;
                                              endif; ?>
                                    </p>
                                    <p>
                                        <?php if(have_rows('telephone')):
                                                    while (have_rows('telephone')): the_row();
                                                        $phone = get_sub_field('telephone');
                                                        $phone = str_replace(" ","",$phone);
                                        ?>
                                                <a href="tel :<?php echo $phone; ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php the_sub_field('telephone')?></a> <br>
                                            <?php       endwhile;
                                                endif; ?>
                                </div>
                                <div class="col-md-8">
                                    <iframe
                                        width="100%"
                                        height="450"
                                        frameborder="0" style="border:0"
                                        src="<?php the_field('google_map_url')?>" allowfullscreen>
                                    </iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
