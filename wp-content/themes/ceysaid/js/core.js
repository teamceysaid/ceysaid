//global variables
$(document).ready(function(){
    /*Slider Prototype*/
    if (!!$.prototype.bxSlider)
        $('.bxsliderTestimonials').bxSlider({
            minSlides: 1,
            maxSlides: 8,
            slideMargin: 20,
            mode: 'vertical',
            pager: true,
            controls: false,
            nextText: 'Next',
            prevText: 'Prev',
            moveSlides:1,
            auto: true,
            infiniteLoop:true,
            hideControlOnEnd: false,
        });
    if (!!$.prototype.bxSlider)
        $('#bxslider1').bxSlider({
            minSlides: 2,
            maxSlides: 6,
            slideWidth: 280,
            slideMargin: 0,
            pager: false,
            nextText: '',
            prevText: '',
            moveSlides:1,
            infiniteLoop:false,
            hideControlOnEnd: true
        });

// Product Page Slider
    $('#views_block').removeClass('hidden');
    var prodThumb = $('#bx-pager_builder a');
    var i = 0;
    while (i <= prodThumb.length) {
        $(prodThumb[i]).attr( 'data-slide-index', i );
        i++;
    }

    if (!!$.prototype.bxSlider)
        var prodSlider = $('#images_list_frame_custom').bxSlider({
            minSlides: 1,
            mode: 'vertical',
            touchEnbabled: true,
            pagerCustom: '#bx-pager_builder',
            infiniteLoop: true,
            auto: 1,
        });
    if (prodThumb.length == 1){
        prodSlider.destroySlider();
    }
    /* Homepage Banner */
    function resize(){
        if ($(window).width() < 480){
            $('#bx-pager_builder').width(($('#bx-pager_builder a').length + 1) * $('#bx-pager_builder a').width());
        }
        var position = $(this).width() < 768 ? $( ".block1" ) : $( ".block2" );
        $( ".block3" ).insertAfter( position );

    }
    resize();
    $(window).resize(resize);
    function reduceName(){
        var conPrice = $(".ajax_block_product");
        for (i = 0; i < conPrice.length; i++ ){
            var founded = conPrice.eq(i).find('.content_price').find('.old-price');

            for (j = 0; j < founded.length; j++){
                var st = conPrice.eq(i).find('.product-name').text();
                st = $.trim(st).substring(0, 11) + "..";
                conPrice.eq(i).find('.product-name').text(st);
            }
        }
    }
    reduceName();
    function bannerPosition(){
        $('.grid-banner').hide();
        var contentBlock = $(".grid-banner");
        var block_positions = {
            "#index .ajax_block_product" : [0, 9] //banners positions on homepage
        }

        for (var b in block_positions){
            var positions = block_positions[b];
            for (i = 0; i<positions.length; i++){
                contentBlock.eq(i).insertBefore( $(b)[positions[i]] ).show();
            }
        }

    }
    bannerPosition();
    /*Menu Labels*/

    var setHot = {
        index: 0, //index of Hot Item
        html: "<span class='menu-label-hot'>Hot</span>"
    };
    var setNew = {
        index: 2, //index of New Item
        html: "<span class='menu-label-new'>New</span>"
    };
    var appd = [setHot, setNew];
    var i=0;
    while (i < appd.length){
        $('.sf-menu > li').eq(appd[i].index).append(appd[i].html);
        i++;
    }
});




