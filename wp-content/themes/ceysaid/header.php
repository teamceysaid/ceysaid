<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/style.css"/>
        <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/slider.css" type="text/css" media="all">
        <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/superfish-modified.css" type="text/css" media="all">
        <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/jquery.bxslider.css" type="text/css" media="all">
        <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/tours.css" type="text/css" media="all">
        <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/extra.css" type="text/css" media="all">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript">
            var homeslider_loop = 1;
            var homeslider_pause = 3000;
            var homeslider_speed = 500;
            var homeslider_width = 345;
        </script>
        <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/core.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/homeslider.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/tours.js"></script>

        <?php wp_head(); ?>
    </head>
    <body>
        <div class="body-overlay"></div>
        <div class="header-container navbar-fixed-top">
            <header id="header">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4 col-sm-3 col-xs-3">
                                <span class="bars">
                                    <span class="bar"></span>
                                    <span class="bar"></span>
                                    <span class="bar"></span>
                                    <span class="bar"></span>

                                </span>
                        </div>
                        <div id="header_logo" class="col-md-4 col-sm-9 col-xs-9">
                            <a href="<?php bloginfo('url'); ?>" title="Ceysaid">
                                <img class="logo img-responsive" src="<?php echo esc_url( get_template_directory_uri() ); ?>/image/logo.png" alt="Ceysaid" width="181" height="100">
                            </a>
                        </div>
                        <div class="col-md-4 hidden-sm hidden-xs">
                                <span class="social_networks">
                                    <?php $phone = get_field('telephone','option');
                                            $phone = str_replace(" ","",$phone);
                                    ?>
                                    <span> <a href="tel:<?php echo $phone;?>"><i class="fa fa-phone"></i> <?php the_field('telephone','option')?></a></span>
                                </span>
                        </div>
                    </div>
                </div>
                <div id="block_top_menu" class="sf-contener clearfix col-lg-12">
                    <?php wp_nav_menu(array('menu' => 'Main Menu','menu_class' => 'sf-menu clearfix menu-content sf-js-enabled sf-arrows')); ?>
                </div>
            </header>
        </div>
