<?php
/**
 * Created by PhpStorm.
 * User: danuja
 * Date: 12/24/2017
 * Time: 1:37 AM
 */
/**
 * Template Name: Main Tours
 */
get_header();
?>
    <div class="columns-container">
        <div id="columns" class="container-fluid">
            <div class="row">
                <div id="center_column" class="center_column col-xs-12 col-sm-12">
                    <ul id="home-page-tabs" class="nav nav-tabs clearfix">
                        <li class="active"><a data-toggle="tab" href="#" class="newarrivals"><?php echo get_the_title()?></a></li>
                    </ul>
                    <div class="tab-content">
                        <ul id="newarrivals" class="tab-pane active product-slider">
                            <?php
                            global $post;
                            if(have_rows('main_tours_list')):
                                while(have_rows('main_tours_list')): the_row();
                                    ?>
                                    <li class="col-md-3 col-sm-6 col-xs-12 products">
                                        <div class="default-box">
                                            <div class="img-container">
                                                <a class="product_img_link" href="<?php the_sub_field('page_url')?>" title="<?php the_sub_field('destination')?>" itemprop="url">
                                                    <img class="replace-2x img-responsive" src="<?php the_sub_field('destination_image')?>" alt="<?php the_sub_field('destination')?>" title="<?php the_sub_field('destination')?>" width="940" height="940" itemprop="image">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="hover-box">
                                            <div class="tour-text">
                                                <a href="<?php the_sub_field('page_url')?>" class="tour-name"><?php the_sub_field('destination')?></a>
                                                <a href="<?php the_sub_field('page_url')?>"><div class="view-more">View More</div></a>
                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                endwhile; //wp_reset_query();
                                wp_reset_postdata();
                            endif;
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>