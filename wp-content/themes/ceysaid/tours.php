<?php
/**
 * Created by PhpStorm.
 * User: danuja
 * Date: 12/24/2017
 * Time: 1:37 AM
 */
/**
 * Template Name: Tours
 */
get_header();
?>
    <div class="columns-container">
        <div id="columns" class="container-fluid">
            <div class="row">
                <div id="center_column" class="center_column col-xs-12 col-sm-12">
                    <ul id="home-page-tabs" class="nav nav-tabs clearfix">
                        <li class="active"><a data-toggle="tab" href="#" class="newarrivals"><?php echo get_the_title()?></a></li>
                    </ul>
                    <div class="tab-content">
                        <ul id="newarrivals" class="tab-pane active product-slider">
                            <?php
                            global $post;
                            if(have_rows('tour_list')):
                                while(have_rows('tour_list')): the_row();
                                    $row = get_sub_field('tours');
                                    $post_id = $row->ID;
                                    $post   = get_post( $post_id );
                                    ?>
                                    <li class="col-md-3 col-sm-6 col-xs-12 products">
                                        <div class="default-box">
                                            <div class="img-container">
                                                <a class="product_img_link" href="<?php the_permalink() ?>" title="Bali Tour" itemprop="url">
                                                    <img class="replace-2x img-responsive" src="<?php the_post_thumbnail('medium'); ?>" alt="<?php the_title() ?>" title="<?php the_title() ?>" width="940" height="940" itemprop="image">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="hover-box">
                                            <div class="tour-text">
                                                <a href="<?php the_permalink() ?>" class="tour-name"><?php the_title() ?></a>
                                                <a href="<?php the_permalink() ?>"><div class="view-more">View More</div></a>
                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                    wp_reset_postdata();
                                endwhile; //wp_reset_query();
                            endif;
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>