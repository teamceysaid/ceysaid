<?php get_header();

?>
<link rel="stylesheet" href="https://uxsolutions.github.io/bootstrap-datepicker/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
<div class="columns-container">
    <div id="columns" class="container-fluid">
        <div class="row">
            <div id="center_column" class="center_column col-xs-12 col-sm-12">
                <div class="primary_block row">
                    <!-- left infos-->
                    <div class="pb-left-column col-xs-12 col-sm-12 col-md-8">
                        <!-- viewws block -->
                        <div id="views_block" class="clearfix">
                            <div id="images_list">
                                <ul id="images_list_frame_custom">
                                    <?php if(have_rows('tours_images')):
                                            while(have_rows('tours_images')): the_row();
                                        ?>

                                        <li>
                                            <img class="img-responsive" src="<?php the_sub_field('images')?>" alt="<?php the_title()?>" title="<?php the_title()?>" height="940" width="940" itemprop="image">
                                        </li>
                                    <?php  endwhile;
                                        endif;?>
                                </ul>
                            </div>

                            <div class="bx-pager_builder__wrapper">
                                <div id="bx-pager_builder">
                                    <?php if(have_rows('tours_images')):
                                        while(have_rows('tours_images')): the_row();
                                            ?>
                                            <div>
                                                <a href="javascript:void(0);" title="NYC Holidays" data-slide-index="0" class="active">
                                                    <img class="img-responsive" src="<?php the_sub_field('images')?>" alt="<?php the_title()?>" title="<?php the_title()?>" height="103" width="103" itemprop="image">
                                                </a>
                                            </div>
                                        <?php  endwhile;
                                    endif;?>
                                </div>
                            </div>
                        </div> <!-- end views-block -->
                        <!-- end thumbnails -->
                    </div> <!-- end pb-left-column -->
                    <!-- end left infos-->
                    <!-- center infos -->
                    <div class="right-col-wrapper col-md-4" id="booking_details">
                        <div class="pb-center-column col-xs-12 col-sm-12 col-md-12">
                            <a class="back-to-category" onclick="history.go(-1);">
                                <i class="fa fa-angle-left"></i> Back
                            </a>
                            <h1 itemprop="name"><?php the_title()?></h1>
                            <div class="content_prices clearfix">
                                <!-- prices -->
                                <div>
                                    <?php $getOffer = get_field('offer_price');
                                        if($getOffer):?>
                                            <p class="our_price_display"><span id="our_price_display" class="price"><?php the_field('offer_price')?></span></p>
                                            <p id="old_price" style="display: inline-block;"><span id="old_price_display"><span class="price"><?php the_field('tour_price')?></span></span></p>
                                    <?php    else:?>
                                            <p class="our_price_display"><span id="our_price_display" class="price"><?php the_field('tour_price')?></span></p>
                                    <?php    endif;
                                    ?>

                                </div> <!-- end prices -->
                                <div class="clear"></div>
                            </div> <!-- end content_prices -->
                            <div id="short_description_block">
                                <div id="short_description_content" class="rte align_justify" itemprop="description">
                                    <p>
                                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                                            <?php the_content(); ?>
                                        <?php endwhile; endif;?>
                                    </p>
                                    <input type="hidden" id="tour_name" value="<?php the_title();?>"/>
                                    <a href="<?php the_field('tour_more_details')?>" target="_blank" id="more_button">More Details</a>
                                </div>
                            </div> <!-- end short_description_block -->
                        </div>
                        <div class="pb-right-column col-xs-12 col-sm-12 col-md-12">
                            <div class="box-info-product">
                                <div class="book-btn">
                                    <button class="btn btn-primary" id="book-btn-click">Book Now</button>
                                </div>
                            </div> <!-- end box-info-product -->
                        </div> <!-- end pb-right-column-->
                    </div>
                    <div class="right-col-wrapper col-md-4" id="booking_form" style="display: none">
                        <div class="pb-center-column col-xs-12 col-sm-12 col-md-12">
                            <a class="back-to-category back-to-details">
                                <i class="fa fa-angle-left"></i> Back
                            </a>
                            <h1>Booking Details</h1>
                            <div id="myBookingForm">
                                <?php echo do_shortcode( '[contact-form-7 id="80" title="Booking Form" html_id="contact_form33"]' ); ?>
                            </div>
                        </div>
                    </div>
                </div> <!-- end primary_block -->
            </div>
        </div>
    </div>
</div>
<script src="https://uxsolutions.github.io/bootstrap-datepicker/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function(){
        var tour_name = $('#tour_name').val();
        $('#tourname').val(tour_name);
        /*
        $('#from_date').on('click',function(){
            alert("ok");
            var popup =$(this).offset();
            console.log(popup.top);
            var popupTop = popup.top + 40;
            $('.datepicker').css({
                'top' : popupTop
            });
        });
        */

        // The date picker (read the docs)
        $('#from_date').datepicker({
            format: "yyyy-mm-dd",
            startDate: 'd',
            container: '#booking_form'
        }).on('changeDate', function(selected){
            $(this).datepicker('hide');
            var minDate = new Date(selected.date.valueOf());
            $('#to_date').datepicker('setStartDate', minDate);
            $("#from_date_label").addClass('active');
        });
        $('#to_date').datepicker({
            format: "yyyy-mm-dd",
            startDate: 'd',
            container: '#booking_form'
        }).on('changeDate', function(selected){
            $(this).datepicker('hide');
            var maxDate = new Date(selected.date.valueOf());
            $('#from_date').datepicker('setEndDate', maxDate);
            $("#to_date_label").addClass('active');
        });
    });
</script>
<?php get_footer(); ?>
