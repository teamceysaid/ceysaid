<?php
/**
 * Created by PhpStorm.
 * User: gihan
 * Date: 12/24/2017
 * Time: 1:16 PM
 */
/**
 * Template Name: About-us
 */
get_header();
?>
    <div class="columns-container cms-about-us" id="cms">
        <div id="columns" class="container-fluid">

            <div id="slider_row" class="row">
            </div>
            <div class="row">
                <div id="center_column" class="center_column col-xs-12 col-sm-12">
                    <ul id="home-page-tabs" class="nav nav-tabs clearfix">
                        <li class="active"><a data-toggle="tab" href="#" class="newarrivals">About us</a></li>
                    </ul>
                    <div class="tab-content"></div>
                    <div class="rte">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <div class="cms-block">
                                    <h3 class="page-subheading">Our company</h3>
                                    <p><strong class="dark"><span><?php the_field("company_moto")?></span><br></strong></p>
                                    <p><?php the_field("company_description")?></p>
                                    <ul class="list-1"><li><em class="icon-ok"></em>Top quality products</li>
                                        <li><em class="icon-ok"></em>Best customer service</li>
                                        <li><em class="icon-ok"></em>30-days money back guarantee</li>
                                    </ul></div>
                            </div>
                            <div class="col-xs-12 col-sm-4" id="parallax-image" style="background-image: url('<?php the_field("background_image")?>')">
                                <div class="cms-box"></div>
                                <div class="cms-about-overlay"></div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="cms-box">
                                    <h3 class="page-subheading">Testimonials</h3>
                                    <ul class="testimonials-ul bxsliderTestimonials" style="width: auto; position: relative; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">
                                        <?php if(have_rows('testimonials')):
                                                    while(have_rows('testimonials')): the_row(); ?>
                                                        <li>
                                                            <div class="testimonials">
                                                                <div class="inner"><span class="before">“</span><?php the_sub_field('client_feedback')?><span class="after">”</span></div>
                                                            </div>
                                                            <p><strong class="dark"><?php the_sub_field('client_name')?></strong></p>
                                                        </li>
                                        <?php       endwhile;
                                               endif;?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>