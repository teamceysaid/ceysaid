<?php
/**
 * Created by PhpStorm.
 * User: danuja
 * Date: 12/23/2017
 * Time: 4:06 PM
 */
/**
 * Template Name: Home
 */
    get_header();
?>
<div class="columns-container">
    <div id="columns" class="container-fluid">
        <div id="slider_row" class="row">
            <div id="top_column" class="center_column col-xs-12 col-sm-12"><!-- Module HomeSlider -->
                <div id="homepage-slider" class="col-xs-12">
                    <ul id="homeslider" style="max-height: 1034px;">
                        <?php
                        if(have_rows('home_slider')):
                            while(have_rows('home_slider')): the_row();
                        ?>
                                <li class="homeslider-container">
                                    <a href="<?php the_sub_field('page_url')?>" title="sample-1">
                                        <img src="<?php the_sub_field('image')?>" width="475" height="1020" alt="<?php the_sub_field('destination')?>">
                                    </a>
                                    <div class="homeslider-description">
                                        <?php if(get_sub_field('hot_tour')):?>
                                            <p class="hot-slider"><span>Hot</span></p>
                                        <?php endif;?>
                                        <h2><?php the_sub_field('destination')?></h2>
                                        <p><?php the_sub_field('small_description')?></p>
                                        <p><button class="btn btn-default" type="button">Book Now!</button></p>
                                    </div>
                                </li>
                        <?php
                            endwhile;
                        endif;
                        ?>
                    </ul>

                </div>
                <!-- /Module HomeSlider -->
            </div>
        </div>
        <div class="row">
            <div id="center_column" class="center_column col-xs-12 col-sm-12">
                <ul id="home-page-tabs" class="nav nav-tabs clearfix">
                    <li class="active"><a data-toggle="tab" href="#newarrivals" class="newarrivals">New arrivals</a></li>
                    <li><a data-toggle="tab" href="#group-departure" class="group-departure">Group Tours</a></li>
                </ul>
                <div class="tab-content">
                    <ul id="newarrivals" class="tab-pane active product-slider">
                        <?php
                        global $post;
                        $args = array(
                            'post_type' => 'tours',
                            'posts_per_page' => -1,
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'tour_type',
                                    'field' => 'slug',
                                    'terms' => 'new_arrivals',
                                )
                            )
                        );
                        $tours = get_posts( $args );

                        if ($tours):
                            foreach ( $tours as $post ) : setup_postdata( $post );
                                ?>
                                <li class="col-md-3 col-sm-6 col-xs-12 products">
                                    <div class="default-box">
                                        <div class="img-container">
                                            <a class="product_img_link" href="<?php the_permalink() ?>" title="Bali Tour" itemprop="url">
                                                <img class="replace-2x img-responsive" src="<?php the_post_thumbnail('medium'); ?>" alt="<?php the_title() ?>" title="<?php the_title() ?>" width="940" height="940" itemprop="image">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="hover-box">
                                        <div class="tour-text">
                                            <a href="<?php the_permalink() ?>" class="tour-name"><?php the_title() ?></a>
                                            <a href="<?php the_permalink() ?>"><div class="view-more">View More</div></a>
                                        </div>
                                    </div>
                                </li>
                            <?php  endforeach; //wp_reset_query();
                            wp_reset_postdata();
                        endif;
                        ?>
                    </ul>
                    <ul id="group-departure" class="tab-pane product-slider">
                        <?php
                        global $post;
                        $args = array(
                            'post_type' => 'tours',
                            'posts_per_page' => -1,
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'tour_type',
                                    'field' => 'slug',
                                    'terms' => 'group-departure',
                                )
                            )
                        );
                        $tours = get_posts( $args );

                        if ($tours):
                            foreach ( $tours as $post ) : setup_postdata( $post );
                                ?>
                                <li class="col-md-3 col-sm-6 col-xs-12 products">
                                    <div class="default-box">
                                        <div class="img-container">
                                            <a class="product_img_link" href="<?php the_permalink() ?>" title="Bali Tour" itemprop="url">
                                                <img class="replace-2x img-responsive" src="<?php the_post_thumbnail('medium'); ?>" alt="<?php the_title() ?>" title="<?php the_title() ?>" width="940" height="940" itemprop="image">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="hover-box">
                                        <div class="tour-text">
                                            <a href="<?php the_permalink() ?>" class="tour-name"><?php the_title() ?></a>
                                            <a href="<?php the_permalink() ?>"><div class="view-more">View More</div></a>
                                        </div>
                                    </div>
                                </li>
                            <?php  endforeach; //wp_reset_query();
                            wp_reset_postdata();
                        endif;
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
