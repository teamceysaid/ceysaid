        <footer>
            <div class="footer">
                <?php wp_nav_menu(array('menu' => 'Bottom Menu','menu_class' => 'footer-links')); ?>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <p> Copyright © Ceysaid 2017. All right reserved. </p>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <ul class="payment">
                            <?php if(get_field('facebook_url','option')){?>
                                <li><a href="<?php the_field('facebook_url','option')?>" target="_blank" class="btn-facebook"><i class="fa fa-facebook"></i> </a></li>
                            <?php }?>
                            <?php if(get_field('twitter_url','option')){?>
                                <li><a href="<?php the_field('twitter_url','option')?>" target="_blank" class="btn-twitter"><i class="fa fa-twitter"></i> </a></li>
                            <?php }?>
                            <?php if(get_field('google_plus_url','option')){?>
                                <li><a href="<?php the_field('google_plus_url','option')?>" target="_blank" class="btn-google-plus"><i class="fa fa-google-plus"></i> </a></li>
                            <?php }?>
                            <?php if(get_field('pinterest_url','option')){?>
                                <li><a href="<?php the_field('pinterest_url','option')?>" target="_blank" class="btn-pinterest"><i class="fa fa-pinterest"></i> </a></li>
                            <?php }?>
                            <?php if(get_field('instagram_url','option')){?>
                                <li><a href="<?php the_field('instagram_url','option')?>" target="_blank" class="btn-instagram"><i class="fa fa-instagram"></i> </a></li>
                            <?php }?>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        Developed by <a href="mailto:ldashanfernando@gmail.com">Danuja Fernando</a>
                    </div>


                </div>
            </div>
            <!--/.footer-bottom-->
        </footer>
        <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/jquery.validate.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/additional-methods.js"></script>

        <script type="text/javascript">
            jQuery(document).ready(function($){
                $.validator.addMethod("regex", function (value, element, regexpr) {
                    return regexpr.test(value);
                }, "");
                $.validator.addMethod("check_number", function (value, element, regexpr) {
                    if(value == ""){
                        return true;
                    }
                    return regexpr.test(value);
                }, "");

                $('#contact_form33').validate({
                    rules: {
                        first_name: {required: true, regex: /^[a-zA-Z]+[a-zA-Z,\s]+\s*$/},
                        last_name: {required: true, regex: /^[a-zA-Z]+[a-zA-Z,\s]+\s*$/},
                        email: {required: true, regex: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/},
                        contact_no: {required: true, minlength: 10, regex: /^[0-9]+$/},
                        adult:{required: true, regex: /^[0-9]+$/},
                        child:{check_number: /^[0-9]+$/},
                        infant:{check_number: /^[0-9]+$/},
                    },
                    messages: {
                        first_name: {required: "Please enter your first name", regex: "Please enter a valid name"},
                        last_name: {required: "Please enter your last name", regex: "Please enter a valid name"},
                        email: {required: "Please enter your email address", regex: "Please enter a valid email address"},
                        contact_no: {required: "Please enter a contact number", minlength: "Please enter valid contact number", regex: "Please enter valid contact number"},
                        adult:{required:"At least include one adult", regex: "Please enter number"},
                        child:{check_number: "Please enter number"},
                        infant:{check_number: "Please enter number"},
                    },
                    submitHandler: function (form) {
                        return true;
                    },
                    invalidHandler: function () {
                        return false;
                    }
                });

                $('.wpcf7 > form#contact_form33 #booking_form_btn').on("click",function(e) {
                    if($('#contact_form33').valid()){
                        return true;
                    }else{
                        return false;
                    }
                });
                $('#contact_form34').validate({
                    rules: {
                        first_name: {required: true, regex: /^[a-zA-Z]+[a-zA-Z,\s]+\s*$/},
                        last_name: {required: true, regex: /^[a-zA-Z]+[a-zA-Z,\s]+\s*$/},
                        email: {required: true, regex: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/},
                        contact_no: {required: true, minlength: 10, regex: /^[0-9]+$/},
                        message:{required: true},
                    },
                    messages: {
                        first_name: {required: "Please enter your first name", regex: "Please enter a valid name"},
                        last_name: {required: "Please enter your last name", regex: "Please enter a valid name"},
                        email: {required: "Please enter your email address", regex: "Please enter a valid email address"},
                        contact_no: {required: "Please enter a contact number", minlength: "Please enter valid contact number", regex: "Please enter valid contact number"},
                        message:{required: "Please enter a you message"},
                    },
                    submitHandler: function (form) {
                        return true;
                    },
                    invalidHandler: function () {
                        return false;
                    }
                });

                $('.wpcf7 > form#contact_form34 #send_inquiry').on("click",function(e) {
                    if($('#contact_form34').valid()){
                        return true;
                    }else{
                        return false;
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.bars').on('click',function(){
                    if($(this).attr('class') == 'bars'){
                        $('.bars').addClass('bars-open');
                        $('.menu-content').css('display','block');
                        $('.body-overlay').addClass('overlayed')
                        $('.body-overlay').css('display','block');
                    }else{
                        $('.bars').removeClass('bars-open');
                        $('.menu-content').css('display','none');
                        $('.body-overlay').removeClass('overlayed');
                        $('.body-overlay').css('display','block');
                    }

                });
                $(".social-show").click(function(){
                    $(".socialsharing_product").toggle();
                });
            });

        </script>
        <?php wp_footer(); ?>
    </body>
</html>